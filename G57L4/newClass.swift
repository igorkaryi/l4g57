//
//  newClass.swift
//  G57L4
//
//  Created by Igor Karyi on 05.10.17.
//  Copyright © 2017 Igor Karyi. All rights reserved.
//

import UIKit

class newClass: NSObject {
    
     static func helloWorld() {
        print("Hello, World!")
    }
    
    static func countHello(count: Int) {
        for _ in 0..<count {
            helloWorld()
            print("Hello, World!")
        }
    }
    
    static func dayWeek() {
    let dayOfTheWeek = arc4random()%7
    
    switch dayOfTheWeek {
    case 0:
    print("Monday")
    case 1:
    print("Tuesday")
    case 2:
    print("Wednesday")
    case 3:
    print("Thursday")
    case 4:
    print("Friday")
    case 5:
    print("Saturday")
    case 6:
    print("Sunday")
    default:
    print("Monday")
    }
    }

    static func arrayDay() {
        
    let days: NSArray = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    let randomNumber = arc4random_uniform(UInt32(days.count))
    let allDays = days[Int(randomNumber)]
    print(allDays)
    }
    
    static func smToDm() {
        let sm:Double = 2.54
        let dm:Double = 4
        let result = sm * dm
        print("\(dm) inch is equal to* \(result) centimeters")
    }
}

